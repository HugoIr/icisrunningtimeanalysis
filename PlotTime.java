import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

import algorithms.Icis;    
import algorithms.InsertionSort;   

public class PlotTime{  

    public static String arrayToSeparatedComma(long[] ar) {
        String result = "";
        int n = ar.length;
        for (int i = 0; i<n-1; i++ ) {
            result += ar[i] + ",";
        }
        result += ar[n-1];

        return result;
    }

    public static void main(String args[]) throws InterruptedException, FileNotFoundException, IOException{  

        int n = 100;
        long[] cordOne = new long[n];
        long[] cordTwo = new long[n];
        ArrayList<Integer> arr = new ArrayList<>();
        int temp = 0;
        Random rand = new Random();
        for (int j = 0; j < n; j++) { 
            temp = rand.nextInt(n);
            arr.add(temp);
            Integer[] tempArr = arr.toArray(new Integer[arr.size()]);

            long start = System.nanoTime();
            InsertionSort.insertionSort(tempArr);
            long end = System.nanoTime();
            long elapsedTime = end-start;
            cordOne[j] = elapsedTime;


            long start2 = System.nanoTime();
            Icis.icisSort(tempArr);
            long end2 = System.nanoTime();
            long elapsedTime2 = end2-start2;
            cordTwo[j] = elapsedTime2;
        }
        BufferedWriter writer = new BufferedWriter(new FileWriter("resultOne.txt"));
        String resultOne = arrayToSeparatedComma(cordOne);
        writer.write(resultOne);
        writer.close();

        BufferedWriter writerTwo = new BufferedWriter(new FileWriter("resultTwo.txt"));
        String resultTwo = arrayToSeparatedComma(cordTwo);
        writerTwo.write(resultTwo);        
        writerTwo.close();
    }  
}  