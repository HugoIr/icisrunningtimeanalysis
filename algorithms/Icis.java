package algorithms;

import java.util.Random;

public class Icis {

    public static int[] icisSort( Integer[] ar) {
        int m = ar.length;
        int min = 0;
        int med = 0;
        int max = 0;

        int[] a = new int[m];

        a[0] = ar[0];

        for (int index = 1; index < m; index++) {
            int x = ar[index];
            // if (x >= a[max] && max < m-1)

            if ( x >= a[max] && max < m-1) {
                max ++;
                a[max] = x;
                med = max/2;
            } 

            // Compare x with the minimum-value in the array minimum-value is usually a [0].
            else if (x <= a[min]) {
                if (max < m-1){
                    max++;
                }
        
                // shift data from min to max
                for (int p = max; p >= 1; p--) {
                    a[p] = a[p-1];
                }
                // insert x into the array
                a[0] = x;
    
                // recalculate the med
                med = max/2;
            } 
                // Check x between the minimum-value and the medium-value in the array    
            else if ( x > a[min] && x < a[med]) {
                if (max < m-1) {
                    max++;
                }

                for (int L = 1; L <= med; L++) {
                    if (x <= a[L]) {
                        // shift data from min to max
                        for (int p = max; p > L; p--) {
                            a[p] = a[p-1];
                        }                
                        a[L] = x;
                        med = max/2;
                        break;
                    }
                }

            }

            else if ( x >= a[med] && x < a[max]) {
                if (max < m-1) {
                    max++;
                }

                for (int L = med; L <= max; L++) {
                    if (x <= a[L]) {
                        for (int p = max; p > L; p-- ) {
                            a[p] = a[p-1];
                        }
                        a[L] = x;                            
                        med = max/2;
                        // int medium = a[med];
                        break;
                    }

                }
            }
        }

        return a;
    }

    // public static void main(String[] args) {
    //     // Integer[] ar = new Integer[]{5,2,4,6,3,2};
    //     Integer[] ar = new Integer[]{33,23,7,80,90,13,88,20,49,9,99,92,94,53,83,62,48,2,6,46,7,83,13,10,38,50,32,38,20,15,53,43,52,67,56,98,38,18,19,73,24,45,15,46,39,74,47,8,4,26,20,42,15,41,42,9,55,79,11,23,34,10,38,29,76,83,36,17,73,82,70,40,61,91,94,3,76,33,21,86,69,28,38,94,79,14,90,97,63,42,44,52,50,9,27,8,29,32,22,74};

    //     int[] res = icisSort(ar);
    //     for (int i : res) {
    //         System.out.print(i + " ");
    //     }
    // }
}